package com.example.demo;

public class Animals {

	protected int age;
	protected String gender;

	public Animals() {
		super();
		this.age = 4;
		this.gender = "male";
	}

	public boolean isMammal() {
		return false;
	}

	public void mate() {
		System.out.println("Animals mate");
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

}