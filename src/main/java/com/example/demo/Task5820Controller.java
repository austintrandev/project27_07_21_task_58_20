package com.example.demo;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Task5820Controller {
	@CrossOrigin
	@GetMapping("/ducks2")
	public Duck getDuck() {
		Duck myDuck = new Duck();
		return myDuck;
	}
	
	@CrossOrigin
	@GetMapping("/fishes2")
	public Fish getFish() {
		return new Fish(5,"female",6,true);
	}
	
	@CrossOrigin
	@GetMapping("/zebras2")
	public Zebra getZebra() {
		return new Zebra();
	}
	
	@CrossOrigin
	@GetMapping("/animals")
	public ArrayList<Animals> getAnimals() {
		ArrayList<Animals> listAnimals = new ArrayList<Animals>();
		Animals myAnimal = new Animals();
		Duck myDuck = new Duck();
		Fish myFish = new Fish(5,"female",6,true);
		Zebra myZebra = new Zebra();
		listAnimals.add(myDuck);
		listAnimals.add(myFish);
		listAnimals.add(myZebra);
		listAnimals.add(myAnimal);
		return listAnimals;
	}
}
